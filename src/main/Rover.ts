import Position from "./Position";
import {
  CommandsInterface,
  MovementInterface,
  DirectionsInterface,
} from "../types/rover.type";

export default class Rover {
  position: Position;
  direction: string;

  constructor(x: number, y: number, direction: string) {
    this.position = new Position(x, y);
    this.direction = direction;
  }

  public getX(): number {
    return this.position.getX();
  }

  public getY(): number {
    return this.position.getY();
  }

  public getDirection(): string {
    return this.direction;
  }

  private setDirection(dir: string) {
    this.direction = dir;
  }

  public move(commandsArray: Array<string>): void {
    const commands: CommandsInterface = {
      F: () => this.goForward(),
      B: () => this.goBackward(),
      L: () => this.turnLeft(),
      R: () => this.turnRigth(),
    };

    commandsArray.forEach((command: string) => {
      commands[command]();
    });
  }

  private goForward() {
    const movement: MovementInterface = {
      N: () => this.position.setY(-1),
      S: () => this.position.setY(1),
      E: () => this.position.setX(1),
      W: () => this.position.setX(-1),
    };

    const direction = this.getDirection();

    movement[direction]();
  }

  private goBackward() {
    const movement: MovementInterface = {
      N: () => this.position.setY(1),
      S: () => this.position.setY(-1),
      E: () => this.position.setX(-1),
      W: () => this.position.setX(1),
    };

    const direction = this.getDirection();

    movement[direction]();
  }

  private turnLeft() {
    const actuallyDirection = this.getDirection();
    const directions: DirectionsInterface = {
      N: () => this.setDirection("W"),
      S: () => this.setDirection("E"),
      W: () => this.setDirection("S"),
      E: () => this.setDirection("N"),
    };
    directions[actuallyDirection]();
  }

  private turnRigth() {
    const actuallyDirection = this.getDirection();
    const directions: DirectionsInterface = {
      N: () => this.setDirection("E"),
      S: () => this.setDirection("W"),
      W: () => this.setDirection("N"),
      E: () => this.setDirection("S"),
    };
    directions[actuallyDirection]();
  }
}
