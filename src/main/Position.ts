
export default class Position {

    x: number;
    y: number;

    constructor(x: number, y: number){
        this.x = x;
        this.y = y;
    }

    public getX(): number{
        return this.x;
    }

    public getY(): number{
        return this.y;
    }

    public setX(num: number){
        if(this.x === 0 && num < 0){
            this.x = 5;
        }else if(this.x === 5 && num > 0){
            this.x = 0;
        }else{
            this.x = this.x + num;
        }
    }

    public setY(num: number){
        if(this.y === 0 && num < 0){
            this.y = 5;
        }else if(this.y === 5 && num > 0){
            this.y = 0;
        }else{
            this.y = this.y + num;
        }
    }
}