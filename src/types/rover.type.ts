export interface CommandsInterface {
    [key: string]: () => void
}

export interface MovementInterface {
    [key: string]: () => void
}

export interface DirectionsInterface {
    [key: string]: () => void
}