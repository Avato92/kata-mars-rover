import Rover from "../main/Rover";

describe("Check Rover class", () => {
  it("constructor works", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "N";
    const rover = new Rover(x, y, direction);

    expect(rover.getX()).toEqual(x);
    expect(rover.getY()).toEqual(y);
    expect(rover.getDirection()).toEqual("N");
  });

  it("Rover recieves commands to move forward", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "N";
    const rover = new Rover(x, y, direction);
    const commands = ["F"];
    rover.move(commands);
    expect(rover.getY()).toEqual(0);
  });

  it("Rover recieves commands to move backward", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "N";
    const rover = new Rover(x, y, direction);
    const commands = ["B"];
    rover.move(commands);
    expect(rover.getY()).toEqual(2);
  });

  it("Rover moves with 2 actions", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "N";
    const rover = new Rover(x, y, direction);
    const commands = ["F", "F"];
    rover.move(commands);
    expect(rover.getY()).toEqual(5);
  });

  it("Rover around the world", () => {
    const x: number = 1;
    const y: number = 5;
    const direction: string = "N";
    const rover = new Rover(x, y, direction);
    const commands = ["B"];
    rover.move(commands);
    expect(rover.getY()).toEqual(0);
  });

  it("Rover around the world in the X axis", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "E";
    const rover = new Rover(x, y, direction);
    const commands = ["B", "B", "F"];
    rover.move(commands);
    expect(rover.getX()).toEqual(0);
  });

  it("Rover turns left", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "E";
    const rover = new Rover(x, y, direction);
    const commands = ["L", "L", "L"];
    rover.move(commands);
    expect(rover.getDirection()).toEqual("S");
  });

  it("Rover turns rigth", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "E";
    const rover = new Rover(x, y, direction);
    const commands = ["R", "R"];
    rover.move(commands);
    expect(rover.getDirection()).toEqual("W");
  });

  it("All together", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "S";
    const rover = new Rover(x, y, direction);
    const commands = ["B", "F", "R", "B", "F"];
    rover.move(commands);
    expect(rover.getDirection()).toEqual("W");
    expect(rover.getX()).toEqual(x);
    expect(rover.getY()).toEqual(y);
  });

  it("Most test to coverage", () => {
    const x: number = 1;
    const y: number = 1;
    const direction: string = "S";
    const rover = new Rover(x, y, direction);
    const commands = ["L"];
    rover.move(commands);
    expect(rover.getDirection()).toEqual("E");
    const commands2 = ["L", "R", "L", "L", "R"];
    rover.move(commands2);
    expect(rover.getDirection()).toEqual("N");
  });
});
